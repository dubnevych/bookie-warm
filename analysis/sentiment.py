import altair as alt
import pandas as pd

from flair.models import TextClassifier
from flair.data import Sentence

from tqdm import tqdm

from .preprocessing import remove_special_characters


def mark_sentiment(classifier, sentences):
    history = []
    for index, line in enumerate(tqdm(sentences)):
        sentence = Sentence(line)
        classifier.predict(sentence)
        for label in sentence.labels:
            if label.value == 'POSITIVE':
                history.append(label.score)
            elif label.value == 'NEGATIVE':
                history.append(1 - label.score)
            else:
                history.append(0.5)

    df = pd.DataFrame({'sentence_id': [x + 1 for x in range(len(history))], 'sentiment': history})

    chapter_size = len(history) // 45
    df['part'] = df.index // chapter_size + 1
    df['part_size'] = chapter_size

    mini_part_size = chapter_size // 10
    df['mini_part'] = ((df.index % chapter_size) // mini_part_size) + 1
    df['mini_part_size'] = mini_part_size

    return df


def aggregate_sentiment(df):
    return df.groupby(['part', 'mini_part']).agg({
        'sentiment': 'mean',
        'part_size': 'max',
        'mini_part_size': 'max',
    }).reset_index()


def build_plot(title, data):
    source = data.groupby('part').agg({'sentiment': 'mean'}).reset_index()
    return alt.Chart(source, title=title).mark_line(
        color=alt.Gradient(
            gradient='linear',
            stops=[alt.GradientStop(color='red', offset=0), alt.GradientStop(color='green', offset=1)],
            x1=1, x2=1,
            y1=1, y2=0,
        )
    ).encode(
        x=alt.X('part:O', axis=alt.Axis(labelAngle=0)),
        y=alt.Y('sentiment:Q'),
        strokeWidth=alt.value(2.5),
    ).configure_title(fontSize=18)


def run_analysis(story, book_content, script_content):
    results = []
    alt.data_transformers.disable_max_rows()

    classifier = TextClassifier.load('sentiment-fast')

    agg_book = None
    book_rects = None
    if book_content is not None:
        book = remove_special_characters(book_content)
        book_df = mark_sentiment(classifier, book)
        agg_book = aggregate_sentiment(book_df)

        book_plot = build_plot(f'{story}: Book Sentiment', agg_book)

        book_rects = alt.Chart(agg_book, title=f'{story}: Detailed Book Sentiment').mark_rect().encode(
            alt.X('mini_part:O', scale=alt.Scale(paddingInner=0), axis=alt.Axis(labelAngle=0)),
            alt.Y('part:O', scale=alt.Scale(paddingInner=0)),
            color=alt.Color('sentiment:Q', scale=alt.Scale(scheme='redblue'))
        )

        results.append(book_plot)
        results.append(book_rects)

    agg_script = None
    script_rects = None
    if script_content is not None:
        script = remove_special_characters(script_content)
        script_df = mark_sentiment(classifier, script)
        agg_script = aggregate_sentiment(script_df)

        script_plot = build_plot('Script Sentiment', agg_script)

        script_rects = alt.Chart(agg_script, title=f'{story}: Detailed Script Sentiment').mark_rect().encode(
            alt.X('mini_part:O', scale=alt.Scale(paddingInner=0), axis=alt.Axis(labelAngle=0)),
            alt.Y('part:O', scale=alt.Scale(paddingInner=0)),
            color=alt.Color('sentiment:Q', scale=alt.Scale(scheme='redblue'))
        )

        results.append(script_plot)
        results.append(script_rects)

    if agg_book is not None and agg_script is not None:
        dfb = agg_book.groupby('part').agg({'sentiment': 'mean'}).reset_index()
        dfs = agg_script.groupby('part').agg({'sentiment': 'mean'}).reset_index()

        agg = dfb.set_index('part').join(dfs.set_index('part'), lsuffix='_book', rsuffix='_script')
        agg = agg[['sentiment_book', 'sentiment_script']]
        agg = agg.rename(columns={'sentiment_book': 'book', 'sentiment_script': 'script'})
        agg = agg.reset_index().melt(id_vars=['part'], var_name='origin', value_name='sentiment')

        agg_map = alt.Chart(agg).mark_line().encode(
            x=alt.X('part:O', axis=alt.Axis(labelAngle=0)),
            y='sentiment:Q',
            color=alt.Color(
                'origin:N',
                scale=alt.Scale(scheme='tableau10'),
                legend=alt.Legend(orient='top', direction='vertical'),
            ),
        )

        results.append(agg_map)

    if book_rects is not None and script_rects is not None:
        results.append(book_rects | script_rects)

    reports = []
    for index, chart in enumerate(results):
        report_name = f'{story}-sentiment-{index}.html'
        chart.save(report_name)
        reports.append(report_name)

    return reports
