import altair as alt
import pandas as pd

from better_profanity import profanity

from tqdm import tqdm

from .preprocessing import remove_special_characters


def mark_profanity(sentences):
    sentence_num = len(sentences)

    prof = []
    for s in tqdm(sentences):
        prof.append(int(profanity.contains_profanity(s)))

    df = pd.DataFrame({'sentence_id': [x + 1 for x in range(sentence_num)], 'profanity': prof})

    chapter_size = sentence_num // 45
    df['part'] = df.index // chapter_size + 1
    df['part_size'] = chapter_size

    mini_part_size = chapter_size // 10
    df['mini_part'] = ((df.index % chapter_size) // mini_part_size) + 1
    df['mini_part_size'] = mini_part_size

    return df


def aggregate_prof(df):
    df_res = df.groupby(['part', 'mini_part']).agg({
        'profanity': 'sum',
        'part_size': 'max',
        'mini_part_size': 'max',
    }).reset_index()
    df_res['normalized_prof'] = df_res['profanity'] / df_res['mini_part_size'] * 100

    return df_res


def run_analysis(story, book_content, script_content):
    results = []
    alt.data_transformers.disable_max_rows()

    book_agg2 = None
    book_rects = None
    if book_content is not None:
        book = remove_special_characters(book_content)
        book_profanity = mark_profanity(book)
        book_agg = aggregate_prof(book_profanity)
        book_agg2 = book_agg.groupby('part').agg({'normalized_prof': 'mean', 'profanity': 'sum'}).reset_index()

        book_map = alt.Chart(book_agg2, title=f'{story}: Book Profanity Percentage').mark_area(
            line={'color': 'darkred'},
            color='red'
        ).encode(x=alt.X('part:O'), y='normalized_prof:Q')

        book_rects = alt.Chart(
            book_agg.loc[book_agg['profanity'] != 0],
            title=f'{story}: Detailed Book Profanity',
        ).mark_rect().encode(
            alt.X('mini_part:O', scale=alt.Scale(paddingInner=0), axis=alt.Axis(labelAngle=0)),
            alt.Y('part:O', scale=alt.Scale(paddingInner=0)),
            color=alt.condition(
                alt.datum.profanity == 0,
                alt.value('white'),
                alt.Color('profanity:Q', scale=alt.Scale(scheme='reds')),
            )
        )

        results.append(book_map)
        results.append(book_rects)

    script_agg2 = None
    script_rects = None
    if script_content is not None:
        script = remove_special_characters(script_content)
        script_profanity = mark_profanity(script)
        script_agg = aggregate_prof(script_profanity)
        script_agg2 = script_agg.groupby('part').agg({'normalized_prof': 'mean', 'profanity': 'sum'}).reset_index()

        script_map = alt.Chart(script_agg2, title=f'{story}: Script Profanity Percentage').mark_area(
            line={'color': 'darkred'},
            color='red'
        ).encode(x='part:O', y='normalized_prof:Q')

        script_rects = alt.Chart(
            script_agg.loc[script_agg['profanity'] != 0],
            title=f'{story}: Detailed Script Profanity',
        ).mark_rect().encode(
            alt.X('mini_part:O', scale=alt.Scale(paddingInner=0), axis=alt.Axis(labelAngle=0)),
            alt.Y('part:O', scale=alt.Scale(paddingInner=0)),
            color=alt.condition(
                alt.datum.profanity == 0,
                alt.value('white'),
                alt.Color('profanity:Q', scale=alt.Scale(scheme='reds')),
            )
        )

        results.append(script_map)
        results.append(script_rects)

    if book_agg2 is not None and script_agg2 is not None:
        agg = book_agg2.set_index('part').merge(
            script_agg2.set_index('part'),
            on=['part'],
            how='inner',
        )

        agg = agg[['normalized_prof_x', 'normalized_prof_y']]
        agg = agg.reset_index()
        agg = agg.rename(columns={'normalized_prof_x': 'book', 'normalized_prof_y': 'script'})
        agg = agg.melt(id_vars=['part'], var_name='origin', value_name='prof')

        agg_map = alt.Chart(agg, title=f'{story}: Profanity Percentage Comparison').mark_area(opacity=0.4).encode(
            x=alt.X("part:O", axis=alt.Axis(labelAngle=0)),
            y=alt.Y("prof:Q", title='profanity', stack=None),
            color=alt.Color("origin:N", scale=alt.Scale(scheme='reds'), legend=alt.Legend(orient='top'))
        )

        results.append(agg_map)

    if book_rects is not None and script_rects is not None:
        results.append(book_rects | script_rects)

    reports = []
    for index, chart in enumerate(results):
        report_name = f'{story}-profanity-{index}.html'
        chart.save(report_name)
        reports.append(report_name)

    return reports
