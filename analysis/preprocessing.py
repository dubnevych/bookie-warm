import string


def get_text(filename: str):
    with open(filename, 'r') as file:
        return file.read()


def remove_special_characters(text: str):
    text = text.replace('\n', ' ')
    text = text.replace('\r', ' ')
    text = text.replace('\'', ' ')
    text = [sentence.strip() for sentence in text.split('.')]
    text = [sentence for sentence in text if sentence != '']
    return text


def clear_token(name: str):
    translation = str.maketrans('', '', string.punctuation)
    return name.strip().translate(translation).lower()


