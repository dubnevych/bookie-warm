from tqdm import tqdm
from collections import Counter

from flair.models import SequenceTagger
from flair.data import Sentence

import altair as alt
import pandas as pd
import numpy as np

from .preprocessing import clear_token, remove_special_characters


def get_distributed_pre_df(tagger, content):
    locations = []
    history = dict()

    # Get all the names of entities tagged as people
    for index, line in enumerate(tqdm(content)):
        sentence = Sentence(line)
        tagger.predict(sentence)
        history[index] = []
        for entity in sentence.get_spans('ner'):
            label = entity.get_label("ner").value
            if label == 'LOC':
                location = clear_token(entity.text)
                locations.append(location)
                history[index].append(location)

    not_locations = ['Hello', 'Okay', 'Peradventure', 'Miss', 'Hmph', 'Busy', 'Aye', 'Yes', 'Flee']
    unique_locations = [loc for loc, c in Counter(locations).most_common() if c > 3 and loc not in not_locations]

    pre_df = []
    for sentence_locations in history.values():
        pre_df.append([sentence_locations.count(location) for location in unique_locations])

    return pre_df, unique_locations


def convert_to_df(pre_df, columns):
    df = pd.DataFrame(pre_df, columns=columns)
    sentence_num = len(df.index)
    chapter_size = sentence_num // 45
    df['part'] = df.index // chapter_size + 1
    df['part_size'] = chapter_size

    return df


def aggregate_history(source_df):
    # Prepare DataFrame for altair
    df = source_df.melt(id_vars=['part', 'part_size'], var_name='location', value_name='occurance')
    df = df.groupby(['part', 'location']).agg({'occurance': 'sum', 'part_size': 'max'}).reset_index()
    df['normalized_occ'] = df['occurance'] / df['part_size'] * 100
    df['normalized_occ'] = df['normalized_occ'].apply(np.ceil)
    df = df.sort_values(by=['part', 'occurance'], ascending=[True, False])

    return df


def get_top_locations(df, n):
    top_locations = df.groupby('location').agg({'normalized_occ': 'sum'}).sort_values(by='normalized_occ',
                                                                                      ascending=False)
    top_locations = top_locations.head(n).reset_index().pop('location')

    return df.loc[df['location'].isin(top_locations)]


def build_plot(title, source):
    base = alt.Chart(source, title=title).encode(
        alt.X('part:O', scale=alt.Scale(paddingInner=0), axis=alt.Axis(labelAngle=0)),
        alt.Y('location:O', scale=alt.Scale(paddingInner=0)),
    )

    # Configure heatmap
    heatmap = base.mark_rect().encode(
        color=alt.condition(
            alt.datum.normalized_occ == 0,
            alt.value('white'),
            alt.Color(
                'normalized_occ:Q',
                scale=alt.Scale(scheme='purples'),
                legend=alt.Legend(title='normalized occurrence', direction='horizontal')
            ),
        )
    )

    text = base.mark_text(baseline='middle').encode(
        text='normalized_occ:Q',
        color=alt.value('white')
    )

    return heatmap + text


def run_analysis(story, book_content, script_content):
    results = []
    alt.data_transformers.disable_max_rows()
    tagger = SequenceTagger.load('ner-fast')

    book_map = None
    if book_content is not None:
        book = remove_special_characters(book_content)
        pre_df_book, book_columns = get_distributed_pre_df(tagger, book)

        df_book = convert_to_df(pre_df_book, book_columns)
        df_book = aggregate_history(df_book)
        df_book = get_top_locations(df_book, 10)

        book_map = build_plot(f'{story}: Book Locations Distribution', df_book)
        results.append(book_map)

    script_map = None
    if script_content is not None:
        script = remove_special_characters(script_content)
        pre_df_script, script_columns = get_distributed_pre_df(tagger, script)

        df_script = convert_to_df(pre_df_script, script_columns)
        df_script = aggregate_history(df_script)
        df_script = get_top_locations(df_script, 10)

        script_map = build_plot(f'{story}: Script Locations Distribution', df_script)
        results.append(script_map)

    if book_map is not None and script_map is not None:
        result = book_map & script_map
        results.append(result.configure_legend(orient='top', direction='horizontal', titleAnchor='middle'))

    reports = []
    for index, chart in enumerate(results):
        report_name = f'{story}-locations-{index}.html'
        chart.save(report_name)
        reports.append(report_name)

    return reports
