from wordcloud import WordCloud
import matplotlib.pyplot as plt


def run_analysis(story, book_content, script_content):
    reports = []
    if book_content is not None:
        wordcloud = WordCloud(background_color='white', collocations=False).generate(book_content)
        plt.imshow(wordcloud, interpolation='bilinear')
        plt.axis("off")

        name = f'{story}-wordcount-book.png'
        wordcloud.to_file(name)
        reports.append(name)

    if script_content is not None:
        wordcloud = WordCloud(background_color='white', collocations=False).generate(script_content)
        plt.imshow(wordcloud, interpolation='bilinear')
        plt.axis("off")

        name = f'{story}-wordcount-script.png'
        wordcloud.to_file(name)
        reports.append(name)

    return reports
