import pyrebase

config = {
    'apiKey': 'AIzaSyCScMlLCYiBXbb7FZre-qPDxygCkQ4z9Ks',
    'authDomain': 'neon-feat-353911.firebaseapp.com',
    'projectId': 'neon-feat-353911',
    'storageBucket': 'neon-feat-353911.appspot.com',
    'messagingSenderId': '856903375742',
    'appId': '1:856903375742:web:bb5f10824f8c47897ab12f',
    'measurementId': 'G-Y83DEHY6LJ',
    'databaseURL': '',
}


class DataService:
    def __init__(self):
        self.firebase = pyrebase.initialize_app(config)
        self.storage = self.firebase.storage()

    def upload(self, cloud_path, local_path):
        self.storage.child(cloud_path).put(local_path)

    def download(self, directory, filepath):
        self.storage.child(f'{directory}/{filepath}').download(filepath)
        with open(filepath, 'r') as file:
            return file.read()
