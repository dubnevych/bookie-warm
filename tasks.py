from celery import Celery
from parsing import DataService
from zipfile import ZipFile

import analysis.characters as characters
import analysis.locations as locations
import analysis.profanity as profanity
import analysis.sentiment as sentiment
import analysis.word_count as word_count

app = Celery(
    'tasks',
    backend='redis://10.189.37.243:6379/0',
    broker='redis://10.189.37.243:6379/1'
)


def _extract_data(data_service, task_options):
    book_path = task_options['book_path']
    script_path = task_options['script_path']

    book = None
    script = None
    name = None
    if book_path is not None:
        book = data_service.download('books', book_path)
        name = book_path.split('.')[0]
    if script_path is not None:
        script = data_service.download('scripts', script_path)
        name = script_path.split('.')[0] if name is None else name

    return name.replace('-', ' '), book, script


def _upload_results(data_service, results):
    folder_name = results[0].split('-')[0]
    zip_name = f"{results[0].split('-')[1]}.zip"
    with ZipFile(zip_name, 'w') as archive:
        for res in results:
            archive.write(res)
    data_service.upload(f'results/{folder_name}/{zip_name}', zip_name)


@app.task
def process_characters(task_options):
    if not task_options['characters']:
        return 'No job'

    try:
        data_service = DataService()
        name, book, script = _extract_data(data_service, task_options)
        results = characters.run_analysis(name, book, script)
        _upload_results(data_service, results)
        return 'Success'
    except Exception as e:
        print(e)
        return 'Failed'


@app.task
def process_locations(task_options):
    if not task_options['locations']:
        return 'No job'

    try:
        data_service = DataService()
        name, book, script = _extract_data(data_service, task_options)
        results = locations.run_analysis(name, book, script)
        _upload_results(data_service, results)
        return 'Success'
    except Exception as e:
        print(e)
        return 'Failed'


@app.task
def process_profanity(task_options):
    if not task_options['profanity']:
        return 'No job'

    try:
        data_service = DataService()
        name, book, script = _extract_data(data_service, task_options)
        results = profanity.run_analysis(name, book, script)
        _upload_results(data_service, results)
        return 'Success'
    except Exception as e:
        print(e)
        return 'Failed'


@app.task
def process_sentiment(task_options):
    if not task_options['sentiment']:
        return 'No job'

    try:
        data_service = DataService()
        name, book, script = _extract_data(data_service, task_options)
        results = sentiment.run_analysis(name, book, script)
        _upload_results(data_service, results)
        return 'Success'
    except Exception as e:
        print(e)
        return 'Failed'


@app.task
def process_word_count(task_options):
    if not task_options['word_count']:
        return 'No job'

    try:
        data_service = DataService()
        name, book, script = _extract_data(data_service, task_options)
        results = word_count.run_analysis(name, book, script)
        _upload_results(data_service, results)
        return 'Success'
    except Exception as e:
        print(e)
        return 'Failed'
